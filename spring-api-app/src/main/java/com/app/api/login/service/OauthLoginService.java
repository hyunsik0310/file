package com.app.api.login.service;

import com.app.domain.member.constant.Role;
import com.app.domain.member.entity.Member;
import com.app.domain.member.service.MemberService;
import com.app.global.jwt.dto.JwtTokenDto;
import com.app.global.jwt.service.TokenManager;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.app.api.external.oauth.model.OAuthAttributes;
import com.app.api.external.oauth.service.SocialLoginApiService;
import com.app.api.external.oauth.service.SocialLoginApiServiceFactory;
import com.app.api.login.dto.OauthLoginDto;
import com.app.domain.member.constant.MemberType;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;

@Service
@Transactional
@Slf4j
@RequiredArgsConstructor
public class OauthLoginService {

    private final MemberService memberService;

    private final TokenManager tokenManager;

    public OauthLoginDto.Response oauthLogin(String accessToken, MemberType memberType) {
        SocialLoginApiService socialLoginApiService =
                SocialLoginApiServiceFactory.getSocialLoginApiSerivce(memberType);

        OAuthAttributes userInfo = socialLoginApiService.getUserInfo(accessToken);

        Optional<Member> optionalMember = memberService.findMemberByEmail(userInfo.getEmail());
        log.info("userInfo : {}", userInfo);

        JwtTokenDto jwtTokenDto;
        Member oauthMember;
        // 新規登録
        if(optionalMember.isEmpty()){
            Member member = userInfo.toMemberEntity(memberType, Role.ADMIN);
            oauthMember = memberService.registerMember(member);
        } else {
            oauthMember = optionalMember.get();
        }

        //トークン生成
        jwtTokenDto = tokenManager.createJwtTokenDto(oauthMember.getMemberId(), oauthMember.getRole());
        oauthMember.updateRefreshToken(jwtTokenDto);

        return OauthLoginDto.Response.of(jwtTokenDto);
    }

}
