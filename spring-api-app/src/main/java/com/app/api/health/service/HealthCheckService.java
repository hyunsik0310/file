package com.app.api.health.service;

import java.util.List;
import com.app.api.health.dto.HealthCheckCountDto;

public interface HealthCheckService {

    public String getResult();

    public List<HealthCheckCountDto> setCountList();

    public void logOutPut(List<HealthCheckCountDto> countList);
}
