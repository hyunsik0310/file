package com.app.api.health.controller;

import java.util.Arrays;
import java.util.List;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.api.health.dto.HealthCheckCountDto;
import com.app.api.health.dto.HealthCheckResponseDto;
import com.app.api.health.service.HealthCheckService;
import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class HealthCheckController {

    private final Environment environment;

    private final HealthCheckService service;

    @GetMapping("/health")
    public ResponseEntity<HealthCheckResponseDto> healthCheck() {
        HealthCheckResponseDto healthCheckResponseDto =
                HealthCheckResponseDto.builder()
                        // .health(service.getResult())
                        .health("ok")
                        .activeProfiles(Arrays.asList(environment.getActiveProfiles()))
                        .build();

        List<HealthCheckCountDto> countList = service.setCountList();
        service.logOutPut(countList);

        return ResponseEntity.ok(healthCheckResponseDto);
    }
}
