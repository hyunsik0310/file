package com.app.api.health.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HealthCheckCountDto {

    private int count;

}
