package com.app.api.health.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HealthCheckResponseDto {

    private String health;

    private List<String> activeProfiles;

}
