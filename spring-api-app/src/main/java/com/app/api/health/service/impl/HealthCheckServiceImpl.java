package com.app.api.health.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;
import com.app.api.health.dto.HealthCheckCountDto;
import com.app.api.health.service.HealthCheckService;

@Service
public class HealthCheckServiceImpl implements HealthCheckService {

    @Override
    public String getResult() {
        return "ok";
    }

    @Override
    public List<HealthCheckCountDto> setCountList() {

        List<HealthCheckCountDto> countList = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            HealthCheckCountDto count = new HealthCheckCountDto();
            count.setCount(i);
            countList.add(count);
        }
        return countList;

    }

    @Override
    public void logOutPut(List<HealthCheckCountDto> countList) {

        for (HealthCheckCountDto count : countList) {
            System.out.println("count : " + count.getCount());
        }

    }

}
