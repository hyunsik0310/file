package com.app.api.exceptiontest.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BindExceptionTestDto {

    @NotBlank(message = "この値は必須です。")
    private String value1;

    @Max(value = 10, message = "最大値は10です。")
    private Integer value2;
}
