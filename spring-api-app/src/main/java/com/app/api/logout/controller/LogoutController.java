package com.app.api.logout.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.app.api.logout.service.LogoutService;
import com.app.global.util.AuthorizationHeaderUtils;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class LogoutController {

    private final LogoutService logoutService;

    @PostMapping("/logout")
    public ResponseEntity<String> logout(HttpServletRequest httpServletRequest) {
        String authHeader = httpServletRequest.getHeader(AuthorizationHeaderUtils.AUTHORIZATION);
        AuthorizationHeaderUtils.validateAuthorization(authHeader);

        String accessToken = AuthorizationHeaderUtils.getToken(authHeader);
        logoutService.logout(accessToken);

        return ResponseEntity.ok("logout success");
    }
}
