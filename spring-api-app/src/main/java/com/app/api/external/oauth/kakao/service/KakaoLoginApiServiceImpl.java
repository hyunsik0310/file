package com.app.api.external.oauth.kakao.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import com.app.api.external.oauth.kakao.client.KakaoUserInfoClient;
import com.app.api.external.oauth.kakao.dto.KakaoUserInfoResponseDto;
import com.app.api.external.oauth.model.OAuthAttributes;
import com.app.api.external.oauth.service.SocialLoginApiService;
import com.app.domain.member.constant.MemberType;
import com.app.global.jwt.constant.GrantType;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
@Transactional
public class KakaoLoginApiServiceImpl implements SocialLoginApiService {

    private final KakaoUserInfoClient kakaoUserInfoClient;

    @Override
    public OAuthAttributes getUserInfo(String accessToken) {

        String contentType = "application/x-www-form-urlencoded;charset=utf8";

        KakaoUserInfoResponseDto kakaoUserInfoResponseDto =
                kakaoUserInfoClient.getKakaoUserInfo(contentType,
                        GrantType.BEARER.getType() + " " + accessToken);
        KakaoUserInfoResponseDto.KakaoAccount kakaoAccount =
                kakaoUserInfoResponseDto.getKakaoAcount();

        String email = kakaoAccount.getEmail();

        return OAuthAttributes.builder()
                .email(!StringUtils.hasText(email) ? kakaoUserInfoResponseDto.getId() : email)
                .name(kakaoAccount.getProfile().getNickname())
                .profile(kakaoAccount.getProfile().getThumbnailImageUrl())
                .memberType(MemberType.KAKAO)
                .build();
    }

}
