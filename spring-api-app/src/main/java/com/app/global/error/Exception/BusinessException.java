package com.app.global.error.Exception;

import com.app.global.error.ErrorCode;
import lombok.Getter;

@Getter
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = -4080696698920688268L;

    private ErrorCode errorCode;

    public BusinessException(ErrorCode errorCode) {

        super(errorCode.getMessage());

        this.errorCode = errorCode;

    }
}
