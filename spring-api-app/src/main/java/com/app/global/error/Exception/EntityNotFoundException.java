package com.app.global.error.Exception;

import com.app.global.error.ErrorCode;

public class EntityNotFoundException extends BusinessException {

    private static final long serialVersionUID = 4791728623903300270L;

    public EntityNotFoundException(ErrorCode errorCode) {
        super(errorCode);
    }
}
