package com.app.global.error;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public enum ErrorCode {

    TEST(HttpStatus.INTERNAL_SERVER_ERROR, "001", "business exception test"),

    // 認証 & 認可
    TOKEN_EXPIRED(HttpStatus.UNAUTHORIZED, "A-001", "トークンが満了されました。"), //
    NOT_VALID_TOKEN(HttpStatus.UNAUTHORIZED, "A-002", "該当トークンは有効なトークンではありません。"), //
    NOT_EXISTS_AUTHORIZATION(HttpStatus.UNAUTHORIZED, "A-003", "AuthorizationHeaderが空です。"), //
    NOT_VALID_BEARER_GRANT_TYPE(HttpStatus.UNAUTHORIZED, "A-004", "認証タイプがBearerタイプではありません。"), //
    REFRESH_TOKEN_NOT_FOUND(HttpStatus.UNAUTHORIZED, "A-005", "該当リフレッシュトークンは存在しません。"),
    REFRESH_TOKEN_EXPIRED(HttpStatus.UNAUTHORIZED, "A-006", "リフレッシュトークンが満了されました。"),
    NOT_ACCESS_TOKEN_TYPE(HttpStatus.UNAUTHORIZED, "A-007", "該当トークンはアクセストークンではありません。"),
    FORBIDDEN_ADMIN(HttpStatus.UNAUTHORIZED, "A-008", "管理者のRoleではないです。"),
    // 会員,
    INVALID_MEMBER_TYPE(HttpStatus.BAD_REQUEST, "M-001", "誤った会員タイプです。"),//,
    ALREADY_REGISTERED_MEMBER(HttpStatus.BAD_REQUEST, "M-002", "既に登録されている会員です"),
    NOT_EXIST_MEMBER(HttpStatus.BAD_REQUEST, "M-003", "該当会員は存在しません。");


    private final HttpStatus httpStatus;
    private final String errorCode;
    private final String message;

    ErrorCode(HttpStatus httpStatus, String errorCode, String message) {
        this.httpStatus = httpStatus;
        this.errorCode = errorCode;
        this.message = message;
    }


}