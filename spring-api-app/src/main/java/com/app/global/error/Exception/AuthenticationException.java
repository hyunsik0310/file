package com.app.global.error.Exception;

import com.app.global.error.ErrorCode;

public class AuthenticationException extends BusinessException {

    private static final long serialVersionUID = 8790754723765201140L;

    public AuthenticationException(ErrorCode err) {
        super(err);
    }
}
