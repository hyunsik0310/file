package com.app.global.util;

import org.springframework.util.StringUtils;
import com.app.global.error.ErrorCode;
import com.app.global.error.Exception.AuthenticationException;
import com.app.global.jwt.constant.GrantType;

public class AuthorizationHeaderUtils {

    public static final String AUTHORIZATION = "Authorization";

    public static void validateAuthorization(String authorizationHeader) {

        // 1. 必須チェック
        if (!StringUtils.hasText(authorizationHeader)) {
            throw new AuthenticationException(ErrorCode.NOT_EXISTS_AUTHORIZATION);
        }
        // 2. Bearerチェック
        String[] authorizations = authorizationHeader.split(" ");
        if (authorizations.length < 2 || (!GrantType.BEARER.getType().equals(authorizations[0]))) {
            throw new AuthenticationException(ErrorCode.NOT_VALID_BEARER_GRANT_TYPE);
        }
    }

    public static String getToken(String header) {
        String token = "";
        try {
            token = header.split(" ")[1];
        } catch (Exception e) {
            throw new AuthenticationException(ErrorCode.NOT_EXISTS_AUTHORIZATION);
        }
        return token;
    }
}
