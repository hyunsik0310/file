package com.app;

import org.jasypt.encryption.pbe.PooledPBEStringEncryptor;
import org.junit.jupiter.api.Test;

public class JasyptTest {

    @Test
    public void jasyptTest() {
        // VM引数の「-Djasypt.password=」の値を記入
        String password = "1234";
        PooledPBEStringEncryptor pooledPBEStringEncryptor = new PooledPBEStringEncryptor();
        pooledPBEStringEncryptor.setPoolSize(4);
        pooledPBEStringEncryptor.setPassword(password);
        pooledPBEStringEncryptor.setAlgorithm("PBEWithMD5AndTripleDES");
        // 暗号化する値「application.ymlにあった元（暗号化前）の値」
        String content = "abcde";
        String encryptedContent = pooledPBEStringEncryptor.encrypt(content);
        String decryptedContent = pooledPBEStringEncryptor.decrypt(encryptedContent);
        System.out.println("Enc : " + encryptedContent + " Dec : " + decryptedContent);
    }
}